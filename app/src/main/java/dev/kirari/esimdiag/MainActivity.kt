package dev.kirari.esimdiag

import android.os.Bundle
import android.telephony.euicc.EuiccManager
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        euicc()
    }

    // android.telephony.euicc  |  Android デベロッパー  |  Android Developers
    // https://developer.android.com/reference/android/telephony/euicc/package-summary
    private fun euicc() {
        val euiccManager: EuiccManager =
                ContextCompat.getSystemService(this, EuiccManager::class.java) ?: return
        if (!euiccManager.isEnabled) {
            Log.d("euicc", "Euicc is not enabled.")
            return
        }
        Log.d("euicc", "Euicc is enabled.")
        val euiccInfo = euiccManager.euiccInfo
        val osVersion = euiccInfo?.osVersion ?: "null"
        Log.d("euicc", "EuiccInfo.osVersion = $osVersion")
    }
}